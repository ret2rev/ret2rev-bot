# ret2rev-bot

## commands

### /ctf add <ctf name> 
create a channel in the active ctfs category and restrict views. ret2rev will be allowed to view by default, other roles may be selected. use is restricted to ret2rev

### /ctf archive <ctf name> 
move the channel for the given ctf into the archive category and allow \@everyone to view it. ctf name may be excluded if used in a ctf channel. use is restricted to ret2rev

### /ctf players add <roles | user>
add selected user/role to the permissions for the ctf. use is restricted to ret2rev

### /ctf players remove <roles | user>
remove selected user/role from the permissions for the ctf. use is restricted to ret2rev

## Environment

Discord bot user token should be defined in environmental variable DISCORD_TOKEN at build time
