mod command;
mod config;
mod ctf;
mod error;
mod util;

use crate::command::{ApplicationCommand, Command, Interaction};
use crate::error::RequestError;
use crate::util::{respond_ephemeral, verify_request};
use aws_lambda_events::encodings::Body;
use netlify_lambda_http::http::StatusCode;
use netlify_lambda_http::{
    lambda::{lambda, Context},
    IntoResponse, Request, Response,
};
use twilight_model::application::callback::InteractionResponse;

fn unsupported_command() -> Result<InteractionResponse, String> {
    Ok(respond_ephemeral("Server doesn't support this command, probably a bug lol".to_string()))
}

async fn dispatch_commands(
    command: &ApplicationCommand,
) -> Result<InteractionResponse, RequestError> {
    let result = match &command.data {
        Command::Ctf(ctf) => ctf::handle_ctf(&command, ctf).await,
        _ => unsupported_command(),
    };
    Ok(match result {
        Ok(x) => x,
        Err(e) => respond_ephemeral(e), // not super likely the user seeing handler errors will be helpful, but might be nice for reporting bugs idk
    })
}

/// wraps the request handling -- Result::ok means the request is valid, even if there is user error
/// Error means we're returning a non-200 response
async fn handle_request(request: &Request) -> Result<InteractionResponse, RequestError> {
    verify_request(&request)?;
    let interaction: Interaction = serde_json::from_str(match request.body() {
        Body::Empty | Body::Binary(_) => Err(RequestError::InvalidPayload(
            "Body is not a string".to_string(),
        )),
        Body::Text(body) => Ok(body),
    }?)
    .map_err(RequestError::JsonFailed)?;
    match interaction {
        Interaction::Ping(_) => Ok(InteractionResponse::Pong), // shouldn't receive pings over webhook
        Interaction::ApplicationCommand(command) => dispatch_commands(&command).await,
    }
}

#[lambda(http)]
#[tokio::main]
async fn main(
    request: Request,
    _: Context,
) -> Result<impl IntoResponse, Box<dyn std::error::Error + Send + Sync + 'static>> {
    match handle_request(&request).await {
        Ok(response) => Ok(Response::builder()
            .status(StatusCode::from_u16(200).unwrap())
            .header("Content-Type", "application/json")
            .body(Body::Text(serde_json::to_string(&response)?))
            .expect("empty 200 response to be valid")),
        Err(e) => Ok(e.into_response()),
    }
}
