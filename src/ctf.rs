use crate::command::ctf::Ctf;
use crate::command::{ctf, ApplicationCommand};
use crate::config::*;
use crate::util::{flatten_name, respond_ephemeral, get_or_create_category};
use twilight_model::application::callback::InteractionResponse;
use twilight_model::channel::permission_overwrite::{PermissionOverwrite, PermissionOverwriteType};
use twilight_model::channel::{Channel, ChannelType, GuildChannel};
use twilight_model::guild::Permissions;
use twilight_model::id::{ChannelId, GuildId, RoleId, UserId};

pub async fn handle_ctf(
    command: &ApplicationCommand,
    ctf: &Ctf,
) -> Result<InteractionResponse, String> {
    let res = match &ctf.options {
        ctf::Options::Add(add) => {
            add_ctf_channel(
                GuildId(
                    command
                        .guild_id
                        .as_ref()
                        .ok_or("dont do it in a dm lol")?
                        .parse::<u64>()
                        .map_err(|x| x.to_string())?,
                ),
                &flatten_name(&add.name),
            )
            .await
        }
        ctf::Options::Archive(archive) => {
            archive_ctf_channel(
                GuildId(
                    command
                        .guild_id
                        .as_ref()
                        .ok_or("dont do it in a dm lol")?
                        .parse::<u64>()
                        .map_err(|x| x.to_string())?,
                ),
                &archive.channel,
            )
            .await
        }
        ctf::Options::Players(players) => {
            let guild_id = GuildId(
                command
                    .guild_id
                    .as_ref()
                    .ok_or("dont do it in a dm lol")?
                    .parse::<u64>()
                    .map_err(|x| x.to_string())?,
            );
            let channel_id = ChannelId(command.channel_id.parse::<u64>().unwrap());
            match players {
                ctf::players::Players::Add(_) => {
                    add_player(guild_id, channel_id, ctf.resolved.as_ref().unwrap()).await
                }
                ctf::players::Players::Remove(_) => {
                    remove_player(guild_id, channel_id, ctf.resolved.as_ref().unwrap()).await
                }
            }
        }
    };
    Ok(respond_ephemeral(match res {
        Ok(x) => x,
        Err(e) => e,
    }))
}

/// create channel and set appropriate permissions
/// by default this is set to allow for config::TEAM_ROLE_ID and deny for config::EVERYONE_ROLE_ID
pub async fn add_ctf_channel(guild: GuildId, name: &str) -> Result<String, String> {
    // 1. check if category channel exists, if not create it
    // 2. create channel by `name` with appropriate permissions
    let client = twilight_http::client::Client::new(crate::config::DISCORD_TOKEN);

    let parent_id: ChannelId = get_or_create_category(guild, ACTIVE_CTF_CATEGORY_NAME)
        .await?
        .id();

    client
        .create_guild_channel(guild, name)
        .map_err(|x| x.to_string())?
        .kind(ChannelType::GuildText)
        .parent_id(parent_id)
        .permission_overwrites(vec![
            PermissionOverwrite {
                deny: Permissions::VIEW_CHANNEL,
                allow: Permissions::empty(),
                kind: PermissionOverwriteType::Role(EVERYONE_ROLE_ID),
            },
            PermissionOverwrite {
                deny: Permissions::empty(),
                allow: Permissions::VIEW_CHANNEL,
                kind: PermissionOverwriteType::Role(TEAM_ROLE_ID),
            },
        ])
        .position(0)
        .await
        .map_err(|x| x.to_string())?;
    Ok(format!("Created {}", name))
}

/// running on a channel will move it to archive category and make it globally viewable
pub async fn archive_ctf_channel(guild: GuildId, id: &str) -> Result<String, String> {
    // 1. check if category channel exists, if not create it
    // 2. move channel by `name`
    // 3. loosen permissions

    let client = twilight_http::client::Client::new(crate::config::DISCORD_TOKEN);

    let parent_id: ChannelId = get_or_create_category(guild, ARCHIVED_CTF_CATEGORY_NAME)
        .await?
        .id();
    let child_id: ChannelId = ChannelId(id.parse::<u64>().map_err(|x| x.to_string())?);
    let text_channel = match client.channel(child_id).await.ok().flatten() {
        Some(Channel::Guild(GuildChannel::Text(text))) => {
            if text.parent_id
                != Some(
                    get_or_create_category(guild, ACTIVE_CTF_CATEGORY_NAME)
                        .await?
                        .id(),
                )
            {
                return Err(format!("Channel {} isn't an active ctf", text.name));
            }
            text
        }
        _ => return Err("Can't archive non-text channel".to_string()),
    };
    client
        .update_channel(child_id)
        .parent_id(parent_id)
        .permission_overwrites(vec![
            PermissionOverwrite {
                deny: Permissions::empty(),
                allow: Permissions::VIEW_CHANNEL,
                kind: PermissionOverwriteType::Role(EVERYONE_ROLE_ID),
            },
            PermissionOverwrite {
                deny: Permissions::SEND_MESSAGES,
                allow: Permissions::empty(),
                kind: PermissionOverwriteType::Role(EVERYONE_ROLE_ID),
            },
        ])
        .await
        .map_err(|x| x.to_string())?;
    Ok(format!("Archived {}", text_channel.name))
}

pub async fn add_player(
    guild: GuildId,
    channel_id: ChannelId,
    resolved: &crate::command::Resolved,
) -> Result<String, String> {
    let (channel, user) = mutate_player_permissions(
        guild,
        channel_id,
        resolved,
        Permissions::VIEW_CHANNEL,
        Permissions::empty(),
    )
    .await?;
    Ok(format!("Added {} to {}", user, channel))
}

pub async fn remove_player(
    guild: GuildId,
    channel_id: ChannelId,
    resolved: &crate::command::Resolved,
) -> Result<String, String> {
    let (channel, user) = mutate_player_permissions(
        guild,
        channel_id,
        resolved,
        Permissions::empty(),
        Permissions::VIEW_CHANNEL,
    )
    .await?;
    Ok(format!("Removed {} from {}", user, channel))
}

async fn mutate_player_permissions(
    guild: GuildId,
    channel_id: ChannelId,
    resolved: &crate::command::Resolved,
    allow: Permissions,
    deny: Permissions,
) -> Result<(String, String), String> {
    let client = twilight_http::client::Client::new(crate::config::DISCORD_TOKEN);
    let channel = client
        .channel(channel_id)
        .await
        .map_err(|x| x.to_string())?
        .expect("calling channel exists");

    let text_channel = match channel {
        Channel::Guild(GuildChannel::Text(text)) => text,
        _ => return Err(format!("Can't add player to non-text channel {}", channel.name().unwrap_or(""))),
    };

    if text_channel.parent_id
        != Some(
            get_or_create_category(guild, ACTIVE_CTF_CATEGORY_NAME)
                .await?
                .id(),
        )
    {
        return Err("Channel isnt an active ctf".to_string());
    };

    // resolved.roles.len() + resolved.members.len() == 1 but we need to handle them differently
    for i in resolved
        .roles
        .keys()
        .filter_map(|x| x.parse::<u64>().ok())
        .map(|x| RoleId(x))
    {
        client
            .update_channel_permission(channel_id, allow, deny)
            .role(i)
            .await
            .map_err(|x| x.to_string())?;
    }
    for i in resolved
        .members
        .keys()
        .filter_map(|x| x.parse::<u64>().ok())
        .map(|x| UserId(x))
    {
        client
            .update_channel_permission(channel_id, allow, deny)
            .member(i)
            .await
            .map_err(|x| x.to_string())?;
    }
    let name = resolved
        .roles
        .values()
        .map(|x| x.name.clone())
        .next()
        .or_else(|| {
            resolved
                .members
                .values()
                .map(|x| x.user.as_ref().unwrap().username.clone())
                .next()
        })
        .expect("either role or user should have string");
    Ok((text_channel.name, name))
}

