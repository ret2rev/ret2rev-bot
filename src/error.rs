use aws_lambda_events::encodings::Body;
use ed25519_dalek::SignatureError;
use hex::FromHexError;
use netlify_lambda_http::http::StatusCode;
use netlify_lambda_http::{IntoResponse, Response};

/// RequestError is used to represent an error in the receiving webhook
/// this only exists because i need to respond with 401 on invalid auth bc discord cares about "security" or something
#[derive(Debug, thiserror::Error)]
pub enum RequestError {
    #[error("Header '{0}' not found.")]
    HeaderNotFound(String),

    #[error("Failed to deserialize from or serialize to JSON.")]
    JsonFailed(#[from] serde_json::Error),

    #[error("Invalid payload provided: {0}.")]
    InvalidPayload(String),

    #[error("Verification failed.")]
    VerificationFailed(VerificationError),
}

#[derive(Debug, thiserror::Error)]
pub enum VerificationError {
    #[error("Failed to parse from hex.")]
    ParseHexFailed(#[from] FromHexError),

    #[error("Invalid public key provided.")]
    InvalidPublicKey(#[from] SignatureError),

    #[error("Invalid signature provided.")]
    InvalidSignature(ed25519_dalek::ed25519::Error),
}

impl IntoResponse for RequestError {
    fn into_response(self) -> Response<Body> {
        Response::builder()
            .status(match self {
                RequestError::HeaderNotFound(_) | RequestError::JsonFailed(_) | RequestError::InvalidPayload(_) => {
                    StatusCode::from_u16(400).unwrap()
                }
                RequestError::VerificationFailed(_) => StatusCode::from_u16(401).unwrap(),
            })
            .body(Body::Empty)
            .expect(
                "implementation of IntoResponse for Error should never return an invalid response",
            )
    }
}
