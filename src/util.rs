use crate::error::{RequestError, VerificationError};
use aws_lambda_events::encodings::Body;
use ed25519_dalek::ed25519::signature::Signature as SignatureFromBytes;
use ed25519_dalek::{PublicKey, Signature, Verifier};
use netlify_lambda_http::Request;
use twilight_model::application::callback::{CallbackData, InteractionResponse};
use twilight_model::channel::message::MessageFlags;
use twilight_model::id::GuildId;
use twilight_model::channel::{ChannelType, GuildChannel};

// blatantly stole the crypto from https://github.com/siketyan/stateless-discord-bot, ty

/// translates arbitrary strings into something that makes a good channel name
/// make ascii lowercase
/// " " -> "-"
/// "\t" -> "-"
pub(crate) fn flatten_name(name: &str) -> String {
    name.to_ascii_lowercase()
        .replace(" ", "-")
        .replace("\t", "-")
}

/// wrapper to generate InteractionResponse from &str
pub(crate) fn respond_ephemeral(message: String) -> InteractionResponse {
    InteractionResponse::ChannelMessageWithSource(CallbackData {
        allowed_mentions: None,
        content: Some(message),
        embeds: vec![],
        flags: Some(MessageFlags::EPHEMERAL),
        tts: None,
    })
}




pub(crate) fn verify_request(request: &Request) -> Result<(), RequestError> {
    let signature = request
        .headers()
        .get("x-signature-ed25519")
        .ok_or_else(||RequestError::HeaderNotFound(
            "x-signature-ed25519".to_string(),
        ))?
        .to_str()
        .expect("headers to be a string");
    let timestamp = request
        .headers()
        .get("x-signature-timestamp")
        .ok_or_else(||RequestError::HeaderNotFound(
            "x-signature-timestamp".to_string(),
        ))?
        .to_str()
        .expect("headers to be a string");
    match request.body() {
        Body::Empty | Body::Binary(_) => Err(RequestError::InvalidPayload(
            "Body is not a string".to_string(),
        )),
        Body::Text(body) => verify_signature(crate::config::DISCORD_PUBLIC_KEY, signature, timestamp, &body)
            .map_err(RequestError::VerificationFailed),
    }
}

pub(crate) fn verify_signature(
    public_key: &str,
    signature: &str,
    timestamp: &str,
    body: &str,
) -> Result<(), VerificationError> {
    let public_key = &hex::decode(public_key)
        .map_err(VerificationError::ParseHexFailed)
        .and_then(|bytes| {
            PublicKey::from_bytes(&bytes).map_err(VerificationError::InvalidSignature)
        })?;

    Ok(public_key.verify(
        format!("{}{}", timestamp, body).as_bytes(),
        &hex::decode(&signature)
            .map_err(VerificationError::ParseHexFailed)
            .and_then(|bytes| {
                Signature::from_bytes(&bytes).map_err(VerificationError::InvalidSignature)
            })?,
    )?)
}

pub(crate) async fn get_or_create_category(guild: GuildId, name: &str) -> Result<GuildChannel, String> {

    async fn get_channel_by_name_and_kind(
        guild: GuildId,
        name: &str,
        kind: ChannelType,
    ) -> Option<GuildChannel> {
        twilight_http::client::Client::new(crate::config::DISCORD_TOKEN)
            .guild_channels(guild)
            .await
            .ok()?
            .into_iter()
            .filter(|x| x.kind() == kind)
            .find(|x| x.name() == name)
    }

    let client = twilight_http::client::Client::new(crate::config::DISCORD_TOKEN);
    Ok(
        match get_channel_by_name_and_kind(guild, name, ChannelType::GuildCategory).await {
            Some(channel) => channel,
            None => client
                .create_guild_channel(guild, name)
                .map_err(|x| x.to_string())?
                .kind(ChannelType::GuildCategory)
                .await
                .map_err(|x| x.to_string())?,
        },
    )
}

