use twilight_model::id::{RoleId};

pub const ACTIVE_CTF_CATEGORY_NAME: &str = "active-ctfs";
pub const ARCHIVED_CTF_CATEGORY_NAME: &str = "archived-ctfs";

// hardcode these bc its constant and saves network time
pub const TEAM_ROLE_ID: RoleId = RoleId(858588254353489940);
pub const EVERYONE_ROLE_ID: RoleId = RoleId(858582445234192465);

pub const DISCORD_PUBLIC_KEY: &str = "e929506d806fbe48d24c88821810dfe96bfac57bb541cbf7884353fcc6aec5c1";
pub const DISCORD_TOKEN: &str = env!("DISCORD_TOKEN");