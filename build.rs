use discord_typed_interactions::Configuration;

fn main() {
    Configuration::new("schema/ctf.json").dest("src/command.rs").generate();
}
