import time
import requests
import os

API_ENDPOINT = 'https://discord.com/api/v8'
CLIENT_ID = '867561056485769226'
with open("secrets/CLIENT_SECRET", "r") as f:
    CLIENT_SECRET = f.read()


def get_token():
    data = {
        'grant_type': 'client_credentials',
        'scope': 'applications.commands applications.commands.update'
    }
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    r = requests.post('%s/oauth2/token' % API_ENDPOINT, data=data, headers=headers, auth=(CLIENT_ID, CLIENT_SECRET))
    r.raise_for_status()
    return r.json()


def upload_schema(token, schema):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer %s' % token
    }
    r = requests.post('%s/applications/%s/commands' % (API_ENDPOINT, CLIENT_ID), data=schema, headers=headers)
    if r.status_code != 200:
        print(r.json())


token = get_token()['access_token']
for i in os.scandir("schema"):
    if i.is_file():
        with open(i, "r") as f:
            json = f.read()
        upload_schema(token, json)
        time.sleep(3)
